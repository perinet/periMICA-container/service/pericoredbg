/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package main

import (
	"testing"

	"gitlab.com/perinet/generic/apiservice/staticfiles"
	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/utils/webhelper"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	"gitlab.com/perinet/periMICA-container/apiservice/network"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
	"gitlab.com/perinet/periMICA-container/apiservice/ssh"
)

func TestMain(t *testing.T) {
	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())
	httpserver.AddPaths(network.PathsGet())
	httpserver.AddPaths(ssh.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(staticfiles.PathsGet())

	staticfiles.Set_files_path("./www")
	staticfiles.Set_expired_path("./www/expired")

	webhelper.InternalPatch(security.Set_webserver_cert_path, []byte("./etc/webserver/"))

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.ListenAndServe("[::1]:50443")
}
