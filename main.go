/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package main

import (
	"encoding/json"
	"log"
	"os"

	"gitlab.com/perinet/generic/lib/utils/webhelper"

	"gitlab.com/perinet/generic/apiservice/staticfiles"
	"gitlab.com/perinet/periMICA-container/apiservice/jlinkdbg"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	"gitlab.com/perinet/periMICA-container/apiservice/network"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
	"gitlab.com/perinet/periMICA-container/apiservice/ssh"

	"gitlab.com/perinet/generic/lib/httpserver"
)

func init() {
	log.SetPrefix("Service PericoredbgContainer: ")

	log.Println("Starting")
	var data []byte
	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "dns-sd", "ssh"}
	data, _ = json.Marshal(services)
	webhelper.InternalPut(node.ServicesSet, data)

	var err error
	data = webhelper.InternalGet(node.NodeInfoGet)
	var nodeInfo node.NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		log.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	// TODO replace old dnssdService with go-avahi implementation

	// //start advertising _https via dnssd
	// dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: "_https._tcp", Port: 443}
	// dnssdServiceInfo.TxtRecord = []string{}
	// dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+nodeInfo.ApiVersion)
	// dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "life_state="+nodeInfo.LifeState)
	// dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
	// dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
	// data, _ = json.Marshal(dnssdServiceInfo)
	// webhelper.InternalVarsPut(dnssd.DNSSDServiceInfoAdvertiseSet, map[string]string{"service_name": dnssdServiceInfo.ServiceName}, data)

	https_service := `<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">

<service-group>

	<name replace-wildcards="yes">%h</name>

	<service>
		<type>_https._tcp</type>
		<port>443</port>
		<txt-record>api_version=` + nodeInfo.ApiVersion + `</txt-record>
		<txt-record>life_state=` + nodeInfo.LifeState + `</txt-record>
		<txt-record>application_name=` + nodeInfo.Config.ApplicationName + `</txt-record>
		<txt-record>element_name=` + nodeInfo.Config.ElementName + `</txt-record>
	</service>

</service-group>`

	f, err := os.Create("/etc/avahi/services/https.service")
	if err != nil {
		log.Println(err)
	}

	_, err2 := f.WriteString(https_service)
	if err2 != nil {
		log.Println(err2)
	}

	f.Close()
}

func main() {
	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())
	httpserver.AddPaths(network.PathsGet())
	httpserver.AddPaths(ssh.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(jlinkdbg.PathsGet())
	// TODO replace old dnssdService with go-avahi implementation
	// httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(staticfiles.PathsGet())

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.ListenAndServe("[::]:443")
}
