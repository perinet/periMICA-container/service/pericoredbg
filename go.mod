module gitlab.com/perinet/periMICA-container/service/pericoredbg

go 1.18

require (
	gitlab.com/perinet/generic/apiservice/staticfiles v0.0.0-20221007144050-1a8dc2782f6a
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20221122163058-59cb209fd3f2
	gitlab.com/perinet/generic/lib/utils v0.0.0-20221122162821-91b714770155
	gitlab.com/perinet/periMICA-container/apiservice/jlinkdbg v0.0.0-20230111142747-1cbfab980af0
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v0.0.0-20221101161231-d517bb9ab0d3
	gitlab.com/perinet/periMICA-container/apiservice/network v0.0.0-20221013145540-d41eb381fc53
	gitlab.com/perinet/periMICA-container/apiservice/node v0.0.0-20221130091942-a456b86bb5af
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20230210151025-9f2dd56cd345
	gitlab.com/perinet/periMICA-container/apiservice/ssh v0.0.0-20221104093456-6bc24bfd2bae
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/tidwall/gjson v1.14.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/exp v0.0.0-20221006183845-316c7553db56 // indirect
	gotest.tools/v3 v3.4.0 // indirect
)
